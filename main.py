import pygame
import random
from pygame.locals import *

pygame.init()

clock = pygame.time.Clock()
fps = 60

screen_width = 1000
screen_height = 700

pygame.time.set_timer(USEREVENT, 800)
pygame.time.set_timer(USEREVENT+1, 8000)
pygame.time.set_timer(USEREVENT+2, 3000)

flip = False
run_game = True
you_lose = False
you_won = False
start_screen = True
player_score = 0
enemy_score = 0
score_font = pygame.font.SysFont("Comic Sans MS", 38)
message_font = pygame.font.SysFont("Comic Sans MS", 68, bold=True)
press_enter_font = pygame.font.SysFont("Comic Sans MS", 48,)
screen_scroll = 0

counter = 0
zero = True

grass_x = screen_width
snake_x = screen_width

screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Bird')

# load background images
bg = pygame.image.load('images/bg.png').convert_alpha()
grass = pygame.image.load('images/grass.png').convert_alpha()
grass.set_colorkey((0, 0, 0))


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.images = []
        for number in range(1, 10):
            img = pygame.image.load(f'images/bird{number}.png')
            img.set_colorkey((0, 0, 0))
            self.images.append(img.convert_alpha())
        self.image = self.images[counter]
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.vel_x = 0
        self.vel_y = 0
        self.acc_y = 0
        self.on_top = False

    def update(self):
        self.vel_x = 0
        self.vel_y = 0

        key = pygame.key.get_pressed()
        if key[pygame.K_SPACE] and self.on_top == False:
            self.acc_y = -15
        self.vel_x += 2

        self.rect.x += self.vel_x
        self.acc_y += 1
        if self.acc_y > 10:
            self.acc_y = 10
        self.vel_y += self.acc_y
        self.rect.y += self.vel_y
        if self.rect.top > 100:
            self.on_top = False
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > screen_width:
            self.rect.right = screen_width
        if self.rect.bottom > screen_height:
            self.rect.bottom = screen_height
            self.vel_y = 0
        if self.rect.top < 0:
            self.rect.top = 0
            self.on_top = True

        key = pygame.key.get_pressed()
        self.image = self.images[counter]
        screen.blit(self.image, self.rect)

        self.rect.x -= self.vel_x
        global screen_scroll
        screen_scroll = -self.vel_x


class Enemy(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.images = []
        for number in range(0, 3):
            img = pygame.image.load(f'images/enemy{number}.png')
            img.set_colorkey((0, 0, 0))
            self.images.append(img.convert_alpha())
        self.image = self.images[counter]
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.vel_x = 0
        self.vel_y = 0
        self.acc_y = 0
        self.count1 = 0
        self.count2 = 0

    def update(self):
        self.vel_y = 0
        if self.rect.y > (screen_height / 4)*3:
            self.acc_y -= 35
        self.acc_y += 1
        if self.acc_y > 10:
            self.acc_y = 10
        self.vel_y += self.acc_y
        self.rect.y += self.vel_y
        self.rect.x += screen_scroll - 2
        self.image = self.images[counter]
        screen.blit(self.image, self.rect)


class Snake(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('images/snake.png').convert_alpha()
        self.image = pygame.transform.scale(self.image, (80, 80))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.img_num = 0

    def update(self):
        self.rect.x += screen_scroll
        if self.rect.x + self.rect.width < 0:
           self.kill()

        if flip == True:
            self.image = pygame.transform.flip(self.image, True, False)


class Fly(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('images/fly.png')
        self.mask = pygame.mask.from_surface(self.image)
        self.image.convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.vel_x = 0
        self.vel_y = 0

    def update(self):
        self.vel_x = 0
        self.vel_y = 0

        if self.rect.x > 0 and self.rect.x < screen_width:
            self.vel_x = random.randint(-2, 2)
        if self.rect.y > 0 and self.rect.y < screen_height:
            self.vel_y = random.randint(-2, 2)

        if self.rect.y <= 1:
            self.vel_y = 4
        if self.rect.y + self.rect.height >= screen_height - 1:
            self.vel_y = -4
        if self.rect.y > screen_height - 178:
            self.rect.y -= 1

        if self.rect.x + self.rect.width < 0:
            self.kill()

        self.rect.x += self.vel_x
        self.rect.y += self.vel_y

        self.rect.x += screen_scroll

        screen.blit(self.image, self.rect)


def spawn_fly():
    fly = Fly(random.randint(screen_width, screen_width + 250),
              random.randint(200, (screen_height - 50) - 128))
    fly_group.add(fly)


def spawn_snake():
    snake = Snake(screen_width, screen_height - 80)
    snake_group.add(snake)


def spawn_enemy():
    enemy = Enemy(screen_width, screen_height / 4)
    enemy_group.add(enemy)


def new_game():
    global player_score, enemy_score, run_game, grass_x
    fly_group.empty()
    snake_group.empty()
    enemy_group.empty()
    player.rect.x = screen_width / 3
    player.rect.y = screen_height / 4
    player_score = 0
    enemy_score = 0
    grass_x = screen_width
    run_game = True


def game_over():
    global run_game
    global you_lose
    global you_won

    run_game = False
    screen.fill((183, 228, 240))
    if you_lose is True:
        game_over_text = message_font.render(str("GAME OVER!"), 1, (0, 0, 0))
        screen.blit(game_over_text, (screen_width / 3.4, screen_height / 3))
        you_lose = False
    if you_won is True:
        game_over_text = message_font.render(str("YOU WON!"), 1, (0, 0, 0))
        screen.blit(game_over_text, (screen_width / 3.4, screen_height / 3))
        you_won = False
    press_enter_text = press_enter_font.render(str("Press ENTER to continue!"), 1, (0, 0, 0))
    screen.blit(press_enter_text, (screen_width / 4.4, screen_height / 1.5))


def start_game():
    screen.fill((183, 228, 240))
    press_enter_text = message_font.render(str("Press ENTER to play!"), 1, (0, 0, 0))
    screen.blit(press_enter_text, (screen_width / 6.5, screen_height / 2.4))


player_group = pygame.sprite.Group()
snake_group = pygame.sprite.Group()
fly_group = pygame.sprite.Group()
enemy_group = pygame.sprite.Group()

player = Player(screen_width / 3, screen_height / 4)
player_group.add(player)


run = True
while run:
    while start_screen == True:
        start_game()
        if pygame.key.get_pressed()[pygame.K_KP_ENTER] or pygame.key.get_pressed()[pygame.K_RETURN]:
            start_screen = False
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
    if run_game == True:
        clock.tick(fps)

        # when event occurs flip snake image
        for event in pygame.event.get():
            if event.type == USEREVENT:
                flip = True
                spawn_fly()
            if event.type == USEREVENT+1:
                spawn_enemy()

        # draw background images
        screen.blit(bg, (0, 0))

        # draw group objects
        player_group.draw(screen)
        snake_group.draw(screen)
        fly_group.draw(screen)
        enemy_group.draw(screen)

        # update group objects
        player_group.update()
        snake_group.update()
        fly_group.update()
        enemy_group.update()

        #print(fly_group.sprites()[0].rect.x)

        if counter == 2 and zero == False:
            counter = 0
        if counter == 1 and zero == False:
            counter += 1
        if counter == 0 and zero == True:
            counter += 1
        if counter == 0 and zero == False:
            zero = True
        if counter == 1 and zero == True:
            zero = False

        snake_x += screen_scroll
        if snake_x < screen_width - 100:
            spawn_snake()
            snake_x = screen_width

        screen.blit(grass, (grass_x, screen_height - 95))
        screen.blit(grass, (grass_x + screen_width, screen_height - 95))
        if grass_x + screen_width < 0:
            grass_x = 0
        grass_x += screen_scroll

        flip = False
        jump = False

        # fly_distance()
        if pygame.sprite.groupcollide(player_group, snake_group, False, False):
            you_lose = True
            game_over()
        if pygame.sprite.groupcollide(player_group, enemy_group, False, False, pygame.sprite.collide_mask):
            you_lose = True
            game_over()
        if pygame.sprite.groupcollide(player_group, fly_group, False, True, pygame.sprite.collide_mask):
                player_score += 1
        if pygame.sprite.groupcollide(enemy_group, fly_group, False, True, pygame.sprite.collide_mask):
                enemy_score += 1
        # while len(fly_group) < 12:
            # spawn_fly()

        # print(fly_group.sprites()[0].rect.center)
        if player.rect.y > screen_height - 95:
            you_lose = True
            game_over()

        if run_game is True:
            player_score_text = score_font.render(str(player_score), 1, (156, 97, 65))
            enemy_score_text = score_font.render(str(enemy_score), 1, (159, 159, 159))
            screen.blit(player_score_text, (100, 20))
            screen.blit(enemy_score_text, (screen_width - 100, 20))

    if pygame.key.get_pressed()[pygame.K_RETURN] and run_game == False:
        new_game()
    if pygame.key.get_pressed()[pygame.K_KP_ENTER] and run_game == False:
        new_game()

    if player_score > 20:
        you_won = True
        game_over()
    if enemy_score > 20:
        you_lose = True
        game_over()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    pygame.display.update()

pygame.quit()
